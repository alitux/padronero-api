from datetime import date
from typing import Optional
from sqlmodel import Field, SQLModel, create_engine, Session, select
from settings import engine

class Escuela(SQLModel, table=True) :
    id: Optional[int] = Field(default=None, primary_key=True)
    nombre: str
    domicilio: str
    localidad: str
    latitud: float
    longitud: float

class Persona(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    nombre: str
    apellido: str
    dni: int
    domicilio: str
    # escuela: str
    fecha_nacimiento: Optional[date] = None
    escuela_id: Optional[int] = Field(default=None, foreign_key="escuela.id")



def create_persona_model(persona: Persona):
    """Crea una persona"""
    with Session(engine) as session:
        session.add(persona)
        session.commit()
        session.refresh(persona)
        return persona

def delete_persona_model(id: int):
    """Recibe un ID y elimina ese item""" 
    with Session(engine) as session:
        persona = session.get(Persona, id)
        if not persona:
            return None
        session.delete(persona)
        session.commit()
        return persona

def drop_persona_model():
    """Borra todos los registros de la tabla personas"""
    with Session(engine) as session:
        personas = session.query(Persona).all()
        for persona in personas:
            session.delete(persona)
        session.commit()
        return len(personas)
        
def create_escuela_model(escuela: Escuela):
    """Crea una escuela"""
    with Session(engine) as session:
        session.add(escuela)
        session.commit()
        session.refresh(escuela)
        return escuela

def drop_escuela_model():
    """Borra todos los registros de la tabla escuelas"""
    with Session(engine) as session:
        escuelas = session.query(Escuela).all()
        for escuela in escuelas:
            session.delete(escuela)
        session.commit()
        return len(escuelas)

def delete_escuela_model(id: int):
    """Recibe un ID y elimina la escuela relacioanada con ese ID"""
    with Session(engine) as session:
        escuela = session.get(Escuela, id)
        if not escuela:
            return None
        session.delete(escuela)
        session.commit()
        return escuela

def query_persona_model(dni: int):
    """ Dado un DNI busca una persona y devuelve todos sus datos """ 
    with Session(engine) as session:
        persona = session.query(Persona).filter(Persona.dni == dni).first()
        if persona:
            escuela = session.query(Escuela).filter(Escuela.id == persona.escuela_id).first()
            return {'persona': persona, 'escuela': escuela}
        return persona

def get_all_personas_model():
    """Devuelve todas las personas del padrón"""
    with Session(engine) as session:
        persons = session.query(Persona).all()
        return persons

def bulk_personas(personas):
    with Session(engine) as session:
        session.add_all([Persona(**persona) for persona in personas])