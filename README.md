# Padronero V1.00

Padronero es una aplicación web desarrollada con FastAPI que te permite administrar un padrón electoral, almacenando información sobre personas y escuelas. También proporciona funcionalidad para cargar datos masivamente desde archivos CSV.

## Requisitos

Asegúrate de tener Python instalado en tu sistema. Puedes instalar las dependencias requeridas utilizando pip:

```bash
pip install -r requirements.txt
```
## Configuración

1. Crea un archivo .env en la raíz del proyecto y configura las variables de entorno necesarias. Ejemplo:

```
DEBUG=True
USERNAME_DB=usuario_postgres
PASSWORD_DB=contraseña_postgres
NAME_DB=nombre_basededatos
URL_DB=localhost
PORT_DB=5432
SECURITY_KEY=UNACADENAALEATORIARANDOM
```
Asegúrate de ajustar los valores según tu entorno y preferencias. DEBUG=True indica que estás en modo de depuración, mientras que en modo de producción, el proyecto utilizará PostgreSQL.

## Uso

- **GET/get_all**: Obtiene una lista con todas las personas registradas en el padrón.
- **GET/query_persona**: Verifica si una persona con un DNI específico existe en la base de datos y devuelve la escuela donde vota.
- **POST/create_persona**: Crea una nueva persona en el padrón.
- **POST/delete_persona**: Elimina una persona por ID.
- **POST/drop_personas**: Borra todas las personas del sistema (¡ten cuidado con esta función!).
- **POST/delete_escuela**: Elimina una escuela por ID.
- **POST/drop_escuelas**: Borra todas las escuelas del sistema (¡ten cuidado con esta función!).
- **POST/create_escuela**: Crea una nueva escuela.
- **POST/upload_personas**: Carga masivamente personas a partir de un archivo CSV válido.
- **POST/upload_escuelas**: Carga masivamente escuelas a partir de un archivo CSV válido.

## Licencia

Este proyecto está bajo la licencia [Licencia GNU/GPLv3](https://www.gnu.org/licenses/gpl-3.0.html)
