import os
import csv
from dotenv import load_dotenv
from io import TextIOWrapper
from fastapi import FastAPI, UploadFile, File, HTTPException
from pydantic import BaseModel
from settings import create_engine, create_db_and_tables, SECURITY_KEY
from models import *


app = FastAPI()

@app.on_event("startup")
def on_startup():
    create_db_and_tables()

@app.get("/get_all")
async def get_all():
    """Devuelve una lista con todas las personas del padrón"""
    return get_all_personas_model()

@app.get("/query_persona")
async def query_persona(dni: int):
    """Dado un DNI, verifica si existe en la base de datos
    y devuelve la escuela donde vota
    """
    persona = query_persona_model(dni)
    return persona

@app.post("/create_persona")
def create_persona(persona: Persona):
    """Crea una persona"""
    persona = create_persona_model(persona)
    return persona

@app.post("/delete_persona")
def delete_persona(id: int):
    """Elimina una persona por ID"""

    persona = delete_persona_model(id)
    return persona

@app.post("/drop_personas")
async def drop_personas(key: str):
    """ATENCION: Borrar todas las personas del sistema""" 
    if key != SECURITY_KEY or not SECURITY_KEY:
        raise HTTPException(status_code=401, detail="Unauthorized")
    drop = drop_persona_model()
    return drop

@app.post("/delete_escuela")
def delete_escuela(id: int):
    """Elimina una escuela por ID"""

    escuela = delete_escuela_model(id)
    return escuela

@app.post("/drop_escuelas")
async def drop_personas(key: str):
    """ATENCION: Borrar todas las escuelas del sistema""" 
    if key != SECURITY_KEY or not SECURITY_KEY:
        raise HTTPException(status_code=401, detail="Unauthorized")
    drop = drop_escuela_model()
    return drop


@app.post("/create_escuela")
def create_escuela(escuela: Escuela):
    """Crea una escuela"""
    escuela = create_escuela_model(escuela)
    return escuela

@app.post("/upload_personas")
async def load_csv(file: UploadFile):
    """Carga masivamente personas a partir de un archivo csv valido
    
    Return: True or False
    """
    header_esperado = list(Persona.__annotations__.keys())[1:] #Crea un header con los fields del modelo Persona
    with TextIOWrapper(file.file, encoding='utf-8') as csv_file:
        cantidad_personas = 0
        csv_reader = csv.reader(csv_file, delimiter=',')
        cabecera_recibida = next(csv_reader)
        if cabecera_recibida != header_esperado: 
            raise HTTPException(status_code=422, detail="Archivo CSV inválido")
        personas = [
            {campo: valor for campo, valor in zip(header_esperado, row)}
            for row in csv_reader
        ]
        for persona in personas:
            create_persona_model(Persona(**persona))
    return f"¡{len(personas)} personas añadidas exitosamente!"

@app.post("/upload_escuelas")
async def upload_escuelas(file: UploadFile):
    """Carga masivamente escuelas a partir de un archivo csv 
    """
    header_esperado = list(Escuela.__annotations__.keys())[1:] #Crea un header con los fields del modelo Escuela
    with TextIOWrapper(file.file, encoding='utf-8') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        cabecera_recibida = next(csv_reader)
        if cabecera_recibida != header_esperado: 
            raise HTTPException(status_code=422, detail="Archivo CSV inválido")
        escuelas = [
            {campo: valor for campo, valor in zip(header_esperado, row)}
            for row in csv_reader
        ]
        for escuela in escuelas:
            create_escuela_model(Escuela(**escuela))

    return f"¡{len(escuelas)} escuelas añadidas exitosamente!"