import os 
from dotenv import load_dotenv
from sqlmodel import SQLModel, create_engine
load_dotenv()

##Variables de Entorno
DEBUG = bool(os.getenv('DEBUG'))
SECURITY_KEY = os.getenv('SECURITY_KEY') #Clave para permitir DROP

if DEBUG is False:
    #Variables de Entorno para Producción con PostgreSQL
    USERNAME_DB = os.getenv('USERNAME_DB')
    PASSWORD_DB = os.getenv('PASSWORD_DB')
    NAME_DB = os.getenv('NAME_DB')
    URL_DB = os.getenv('URL_DB')
    PORT_DB = os.getenv('PORT_DB')

if DEBUG is True:
    sqlite_file_name = "sqlite.db"
    sqlite_url = f"sqlite:///{sqlite_file_name}"
    engine = create_engine(sqlite_url, echo=True)

if DEBUG is False:
    DATABASE_URL = f"postgresql://{{USERNAME_DB}}:{{PASSWORD_DB}}@{{URL_DB}}:{{PORT_DB}}/{{NAME_DB}}"
    engine = create_engine(DATABASE_URL)

def create_db_and_tables():
    """Crea la base de datos y sus tablas"""
    SQLModel.metadata.create_all(engine)